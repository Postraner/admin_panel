import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import * as authActions from 'redux/modules/auth';

@connect(
  state => ({user: state.auth.user}),
  authActions)
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const user = {
      name: this.refs.username.value,
      pass: this.refs.password.value
    };
    this.props.login(user);
  };

  render() {
    const styles = require('./Login.scss');
    return (
      <div className={styles.loginPage}>
        <div className={styles.loginWrapper}>
          <div className={styles.logoWrapper}>
            <svg xmlns="http://www.w3.org/2000/svg" width="168" height="120" viewBox="0 0 168 120"><path fill="#494949" d="M107.81 0A59.63 59.63 0 0 0 59.5 24.62a51.67 51.67 0 0 1 90.18 34.43 51.67 51.67 0 0 1-51.66 51.69A51.6 51.6 0 0 1 55 87.66a59.7 59.7 0 0 0 112.52-27.91c0-33-26.73-59.75-59.71-59.75M0 61.74h4.74c0 4.07 1.62 5.7 5.54 5.7 1.89 0 4.27-1.19 4.27-3.33 0-7.88-14.43-1.98-14.43-12.59C.12 46.49 3.79 44 8.5 44c2.53 0 4.62.48 6.2 2.65h.08v-2.1h4.55v8.08h-4.74c-.2-3.48-1.55-5.07-5.1-5.07-2.22 0-4.11.95-4.11 3.37C5.38 56.95 20 52.51 20 63.2c0 4.87-3.72 7.8-8.42 7.8-2.69 0-5.26-.32-6.96-2.69h-.07v2.14H0z"/><path fill="#494949" d="M59 47.63h-1.74L51.92 69h-6.64l-4.76-19.72h-.08L35.49 69h-6.65l-5.1-21.37H22V44h10.52v3.63h-3.48l3.32 15.79h.08L37.35 44h6.88l4.64 19.42h.08l3.59-15.79h-3.48V44H59zM70.95 56.04h2.9c3.06 0 5.73-.53 5.73-4.13 0-4.32-2.98-4.28-5.92-4.28h-2.71zM62 65.37h3.53V47.63H62V44h13.85c4.05 0 9.15.76 9.15 7.65 0 6.42-3.96 8.02-9.15 8.02h-4.9v5.7h4.12V69H62zM110 44v8.64h-4.44v-5.01h-4.4v17.74h3.67V69H92.17v-3.63h3.67V47.63h-4.4v5.01H87V44zM129.11 57.72l-3.84-9.59-3.92 9.59zM111 65.37h2.16L122.21 44h6.66l8.97 21.37H140V69h-11.01v-3.63h3.21l-1.68-4.02H119.9l-1.61 4.02h3.21V69H111z"/><path fill="#f20c2a" d="M1 60.74h4.74c0 4.07 1.62 5.7 5.54 5.7 1.89 0 4.27-1.19 4.27-3.33 0-7.88-14.43-1.98-14.43-12.59C1.12 45.49 4.79 43 9.5 43c2.53 0 4.62.48 6.2 2.65h.08v-2.1h4.55v8.08h-4.74c-.2-3.48-1.55-5.07-5.1-5.07-2.22 0-4.11.95-4.11 3.37C6.38 55.95 21 51.51 21 62.2c0 4.87-3.72 7.8-8.42 7.8-2.69 0-5.26-.32-6.96-2.69h-.07v2.14H1zM60 47.63h-1.74L52.92 69h-6.65l-4.75-19.72h-.08L36.49 69h-6.65l-5.1-21.37H23V44h10.52v3.63h-3.48l3.32 15.79h.08L38.35 44h6.88l4.64 19.42h.08l3.59-15.79h-3.48V44H60zM71.95 56.04h2.9c3.06 0 5.73-.53 5.73-4.13 0-4.32-2.98-4.28-5.92-4.28h-2.71zM63 65.37h3.53V47.63H63V44h13.85c4.05 0 9.15.76 9.15 7.65 0 6.42-3.96 8.02-9.15 8.02h-4.9v5.7h4.12V69H63zM111 44v8.64h-4.44v-5.01h-4.4v17.74h3.67V69H93.17v-3.63h3.67V47.63h-4.4v5.01H88V44zM130.11 57.72l-3.84-9.59-3.92 9.59zM112 65.37h2.16L123.21 44h6.66l8.97 21.37H141V69h-11.01v-3.63h3.21l-1.68-4.02H120.9l-1.61 4.02h3.21V69H112z"/></svg>
          </div>
          <div className={styles.formWrapper}>
            <form onSubmit={this.handleSubmit}>
              <div className={styles.inputWrapper}>
                <input type="email" ref="username" placeholder="Email" />
              </div>
              <div className={styles.inputWrapper}>
                <input type="pass word" ref="password" placeholder="Password" />
              </div>
              <div className={styles.btnBox}>
                <a href="#!" className={styles.forgotPassword}>Forgot password?</a>
                <input type="submit" value="LOG IN" className={styles.loginBtn} onClick={this.handleSubmit}/>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
